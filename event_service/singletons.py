import logging

import aio_pika
from pydantic import ValidationError

from .api_integrations import get_api_v1_integration
from .empaia_sender_auth import AioHttpClient, AuthSettings
from .settings import Settings
from .validation_emails import TokenValidator

settings = Settings()

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
if settings.debug:
    logger.setLevel(logging.DEBUG)

api_v1_integration = get_api_v1_integration(settings, logger)

auth_settings = None
try:
    auth_settings = AuthSettings(
        idp_url=settings.idp_url, client_id=settings.client_id, client_secret=settings.client_secret
    )
except ValidationError as e:
    logger.info("Auth not configured: %s", e)

http_client = AioHttpClient(
    logger=logger,
    auth_settings=auth_settings,
    chunk_size=settings.connection_chunk_size,
    timeout=settings.connection_timeout,
)


async def _get_connection():
    return await aio_pika.connect_robust(
        host=settings.amqp_host,
        port=settings.amqp_port,
        login=settings.amqp_username,
        password=settings.amqp_password,
        virtualhost=settings.amqp_virtualhost,
        ssl=settings.amqp_ssl,
    )


pika_pool = aio_pika.pool.Pool(_get_connection)

sos_url = settings.sos_url.rstrip("/")

token_validator = TokenValidator(
    settings=settings,
    http_client=http_client,
    logger=logger,
    sos_url=sos_url,
)
