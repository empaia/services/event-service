from fastapi import Depends, HTTPException, Request
from fastapi.openapi.models import OAuthFlowAuthorizationCode, OAuthFlows
from fastapi.security import OAuth2
from pydantic_settings import BaseSettings, SettingsConfigDict

from ....empaia_receiver_auth import Auth


class AuthSettings(BaseSettings):
    idp_url: str
    audience: str = ""
    refresh_interval: int = 300  # seconds
    openapi_token_url: str = ""
    openapi_auth_url: str = ""
    rewrite_url_in_wellknown: str = ""

    model_config = SettingsConfigDict(env_file=".env", env_prefix="EVES_", extra="ignore")


def make_oauth2_wrapper(auth: Auth, auth_settings: AuthSettings):
    oauth2_scheme = OAuth2(
        flows=OAuthFlows(
            authorizationCode=OAuthFlowAuthorizationCode(
                tokenUrl=auth_settings.openapi_token_url, authorizationUrl=auth_settings.openapi_auth_url
            )
        )
    )

    def oauth2_wrapper(request: Request, token=Depends(oauth2_scheme)):
        decoded_token = auth.decode_token(token)
        # check whether additional user-id header matches the client id
        if request.headers.get("user-id") != decoded_token.get("sub"):
            raise HTTPException(status_code=412, detail="User-ID header does not match Client ID")
        return decoded_token

    return oauth2_wrapper


class EmpaiaApiIntegration:
    def __init__(self, settings, logger):
        self.settings = settings
        self.logger = logger

        self.auth_settings = AuthSettings()

        audience = self.auth_settings.audience
        if audience == "":
            audience = None

        self.auth = Auth(
            idp_url=self.auth_settings.idp_url.rstrip("/"),
            refresh_interval=self.auth_settings.refresh_interval,
            audience=audience,
            logger=self.logger,
            rewrite_url_in_wellknown=self.auth_settings.rewrite_url_in_wellknown,
        )
        self.oauth2_wrapper = make_oauth2_wrapper(auth=self.auth, auth_settings=self.auth_settings)

    def global_depends(self):
        return Depends(self.oauth2_wrapper)
