from fastapi import Header, HTTPException

from ...models import Notification, NotificationList, PutConfirm
from ...singletons import api_v1_integration, http_client, logger, sos_url


def add_routes_notifications(app):
    @app.get(
        "/notifications",
        response_model=NotificationList,
        tags=["Notifications"],
    )
    async def _(
        user_id: str = Header(...),
        _=api_v1_integration.global_depends(),
    ) -> NotificationList:
        query = {
            "recipients": [user_id],
            "statuses": ["UNCONFIRMED"],
        }

        shout_outs = await http_client.put(f"{sos_url}/v1/shout-outs/query", json=query)

        notifications = []
        for shout_out in shout_outs["items"]:
            for message in shout_out["messages"]:
                if message["sink"] != "AMQP":
                    continue

                notification = Notification(
                    id=message["id"],
                    content=message["content"],
                    created_at=shout_out["created_at"],
                )
                notifications.append(notification)

        return NotificationList(item_count=len(notifications), items=notifications)

    @app.put(
        "/notifications/confirm",
        response_model=NotificationList,
        tags=["Notifications"],
    )
    async def _(
        put_confirm: PutConfirm,
        user_id: str = Header(...),
        _=api_v1_integration.global_depends(),
    ) -> NotificationList:
        query = {
            "recipients": [user_id],
            "statuses": ["UNCONFIRMED"],
        }

        shout_outs = await http_client.put(f"{sos_url}/v1/shout-outs/query", json=query)

        notifications = []
        found_id = False
        notification_id = str(put_confirm.notification_id)
        for shout_out in shout_outs["items"]:
            for message in shout_out["messages"]:
                if message["sink"] != "AMQP":
                    continue
                if message["id"] == notification_id:
                    found_id = True
                    continue

                notification = Notification(
                    id=message["id"],
                    content=message["content"],
                    created_at=shout_out["created_at"],
                )
                notifications.append(notification)

        if not found_id:
            raise HTTPException(status_code=400, detail=f"Could not confirm notification with ID {notification_id}.")

        try:
            data = {"message_id": notification_id}
            _ = await http_client.put(f"{sos_url}/v1/shout-outs/confirm", json=data)
        except HTTPException as e:
            logger.info("SOS Error: %s", e)
            raise HTTPException(
                status_code=400, detail=f"Could not confirm notification with ID {notification_id}."
            ) from e

        if put_confirm.trigger_event:
            try:
                data = {
                    "data": {
                        "event": "NOTIFICATION",
                        "event_data": {"action": "UPDATED"},
                    },
                    "routing_key": user_id,
                }
                _ = await http_client.put(f"{sos_url}/v1/direct/amqp", json=data)
            except HTTPException as e:
                logger.error("SOS Error: %s", e)
                raise HTTPException(status_code=400, detail="Could not trigger event.") from e

        return NotificationList(item_count=len(notifications), items=notifications)
