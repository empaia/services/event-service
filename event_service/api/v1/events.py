import asyncio
import json

from fastapi import Header
from sse_starlette.sse import EventSourceResponse

from ...singletons import api_v1_integration, pika_pool


def add_routes_events(app):
    @app.get(
        "/events",
        response_model=None,
        tags=["Events"],
    )
    async def _(
        user_id: str = Header(...),
        _=api_v1_integration.global_depends(),
    ) -> None:
        async def event_generator():
            async with pika_pool.acquire() as connection:
                async with connection.channel() as channel:
                    await channel.set_qos(prefetch_count=1)
                    queue = await channel.declare_queue(user_id, exclusive=False, auto_delete=True)

                    while True:
                        message = await queue.get(fail=False, no_ack=True)

                        if not message:
                            await asyncio.sleep(1)
                            continue

                        data = json.loads(message.body.decode("utf-8"))
                        if data.get("message_id"):
                            event_data = {"action": "CREATED"}
                            yield {"event": "NOTIFICATION", "data": json.dumps(event_data)}
                        elif data.get("event"):
                            event_data = None
                            if "event_data" in data:
                                event_data = json.dumps(data["event_data"])
                            yield {"event": data["event"], "data": event_data}

        return EventSourceResponse(event_generator())
