from fastapi import HTTPException

from ...models import PutEmailConfirm
from ...singletons import http_client, logger, sos_url, token_validator


def add_routes_emails(app):
    @app.put(
        "/emails/confirm",
        response_model=None,
        tags=["Emails"],
    )
    async def _(
        put_email_confirm: PutEmailConfirm,
    ) -> None:
        payload = await token_validator.validate_confirm_token(put_email_confirm.confirm_token)
        data = {"message_id": payload["message_id"]}

        try:
            await http_client.put(f"{sos_url}/v1/shout-outs/confirm", json=data)
        except HTTPException as e:
            logger.error("SOS Error: %s", e)
            raise HTTPException(400, "Could not confirm email.") from e

        if put_email_confirm.trigger_event:
            try:
                data = {
                    "data": {
                        "event": "NOTIFICATION",
                        "event_data": {"action": "UPDATED"},
                    },
                    "routing_key": payload["sub"],
                }
                _ = await http_client.put(f"{sos_url}/v1/direct/amqp", json=data)
            except HTTPException as e:
                logger.error("SOS Error: %s", e)
                raise HTTPException(status_code=400, detail="Could not trigger event.") from e
