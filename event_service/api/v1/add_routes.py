from .emails import add_routes_emails
from .events import add_routes_events
from .notifications import add_routes_notifications


def add_routes_v1(app):
    add_routes_notifications(app)
    add_routes_events(app)
    add_routes_emails(app)
