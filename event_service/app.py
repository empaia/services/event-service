from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__ as version
from .api.v1.add_routes import add_routes_v1
from .singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""


app = FastAPI(
    title="Event Service",
    version=version,
    redoc_url=None,
    openapi_url=openapi_url,
    root_path=settings.root_path,
)

if settings.cors_allow_origins:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors_allow_origins,
        allow_credentials=settings.cors_allow_credentials,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.get("/alive", tags=["Private"])
async def _():
    return {"status": "ok", "version": version}


app_v1 = FastAPI(openapi_url=openapi_url)
add_routes_v1(app_v1)
app.mount("/v1", app_v1)
