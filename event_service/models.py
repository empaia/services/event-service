from typing import List

from pydantic import UUID4, BaseModel, ConfigDict, Field, conint

Timestamp = conint(ge=0)
ItemCount = conint(ge=0)


class RestrictedBaseModel(BaseModel):
    """Abstract Super-class not allowing unknown fields in the **kwargs."""

    model_config = ConfigDict(extra="forbid")


class Notification(RestrictedBaseModel):
    id: UUID4
    content: str
    created_at: Timestamp

    __hash__ = object.__hash__


class NotificationList(RestrictedBaseModel):
    item_count: ItemCount = Field(
        examples=[1],
        description="Number of Notifications.",
    )

    items: List[Notification] = Field(
        examples=[
            [
                Notification(
                    id="a10648a7-340d-43fc-a2d9-4d91cc86f33f",
                    content="Hello Rabbit!",
                    created_at=1623349180,
                )
            ]
        ],
        description="List of Notification items.",
    )


class PutConfirm(RestrictedBaseModel):
    notification_id: UUID4
    trigger_event: bool = False


class PutEmailConfirm(RestrictedBaseModel):
    confirm_token: str
    trigger_event: bool = False
