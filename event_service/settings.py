from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    connection_chunk_size: int = 1024000
    connection_timeout: int = 300
    disable_openapi: bool = False
    root_path: str = ""
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    api_v1_integration: str = ""
    debug: bool = False
    sos_url: str = "http://sos"
    amqp_host: str = "localhost"
    amqp_port: int = 5672
    amqp_username: str = "guest"
    amqp_password: str = "guest"
    amqp_virtualhost: str = "/"
    amqp_ssl: bool = False
    idp_url: str
    client_id: str = ""
    client_secret: str = "s"
    email_validation_key_fresh_interval: int = 300

    model_config = SettingsConfigDict(env_file=".env", env_prefix="EVES_", extra="ignore")
