import json
from time import time

from fastapi import HTTPException, status

from .empaia_sender_auth import AioHttpClient
from .settings import Settings
from .validation_helpers import validate_token


class TokenValidator:
    def __init__(self, settings: Settings, http_client: AioHttpClient, logger, sos_url):
        self._settings = settings
        self._http_client = http_client
        self._logger = logger
        self._sos_url = sos_url

        self._verification_key: str = None
        self._last_key_refresh_time: float = None

    async def _ensure_fresh_verification_key(self):
        # The verification key must be regularly refreshed in case the examination service updates it,
        # otherwise the scope token validation will fail.
        initialized = None not in (self._verification_key, self._last_key_refresh_time)
        key_expired = False
        if initialized:
            key_expired = (
                time().real - self._last_key_refresh_time
            ) > self._settings.email_validation_key_fresh_interval
        if (not initialized) or key_expired:
            try:
                self._verification_key = await self._fetch_raw_public_key()
            except HTTPException as e:
                self._logger.error(
                    "Could not retrieve public key for token validation from Shout Out Service "
                    f"(http status: {e.status_code}):\n{e.detail}"
                )
            except Exception as e:
                self._logger.error(f"Could not retrieve public key for token validation from Shout Out Service: {e}")
            else:
                self._last_key_refresh_time = time().real

    async def _get_verification_key(self) -> bytes:
        await self._ensure_fresh_verification_key()
        return self._verification_key

    async def validate_confirm_token(self, confirm_token: str):
        key = await self._get_verification_key()
        if key is None:
            raise HTTPException(
                status_code=401,
                detail="Email token confirmation not initialized on server.",
            )
        payload = validate_token(confirm_token, key)
        self._validate_claims(payload)
        return payload

    async def _fetch_raw_public_key(self) -> bytes:
        url = f"{self._sos_url}/v1/public-key"
        data = await self._http_client.get(url)
        return data.encode("ascii")

    def _validate_claims(self, payload: dict):
        if "sub" not in payload or "message_id" not in payload or "sink" not in payload or payload["sink"] != "SMTP":
            self._logger.error("%s", json.dumps(payload))
            raise HTTPException(status_code=status.HTTP_412_PRECONDITION_FAILED)
