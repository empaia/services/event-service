import json

import requests

from .settings import Settings

settings = Settings()
eves_url = settings.eves_url.strip("/")


EXCLUDED_PATHS = ["/emails/confirm"]


def test_routes_v1():
    r = requests.get(f"{eves_url}/v1/openapi.json")
    openapi = json.loads(r.content)
    for path, ops in openapi["paths"].items():
        if path in EXCLUDED_PATHS:
            continue

        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{eves_url}/v1{path_no_vars}"
        for op in ops.keys():
            print(op, url)
            r = requests.request(op, url)
            print(r.text)
            assert r.status_code == 403
