from .settings import Settings

settings = Settings()
sos_url = settings.sos_url.rstrip("/")
eves_url = settings.eves_url.rstrip("/")
maildev_url = settings.maildev_url.rstrip("/")
