import json
from time import sleep
from uuid import uuid4

import requests
import sseclient

from event_service.models import NotificationList

from .singletons import eves_url, sos_url


def test_put_direct_amqp():
    random_recipient = str(uuid4())
    sse_headers = {
        "user-id": random_recipient,
        "Accept": "text/event-stream",
    }
    headers = {
        "user-id": random_recipient,
    }

    r = requests.get(f"{eves_url}/v1/events", headers=sse_headers, stream=True)
    r.raise_for_status()
    client = sseclient.SSEClient(r)
    sleep(1)

    put_direct_amqp = {
        "data": {"event": "PORTAL_APP", "event_data": {"action": "CREATED"}},
        "routing_key": random_recipient,
    }

    r = requests.put(f"{sos_url}/v1/direct/amqp", json=put_direct_amqp)
    r.raise_for_status()

    message = {
        "sink": "AMQP",
        "content": "Hello Rabbit!",
    }

    post_shout_out = {
        "recipient_id": random_recipient,
        "timeout_interval": 60,
        "messages": [message],
    }

    r = requests.post(f"{sos_url}/v1/shout-outs", json=post_shout_out)
    print(r.text)
    r.raise_for_status()

    events = []

    for event in client.events():
        if event.event == "ping":
            continue

        events.append(event.event)
        if len(events) == 2:
            break

    assert list(sorted(events)) == list(sorted(["PORTAL_APP", "NOTIFICATION"]))

    r = requests.get(f"{eves_url}/v1/events", headers=sse_headers, stream=True)
    r.raise_for_status()
    client = sseclient.SSEClient(r)

    r = requests.get(f"{eves_url}/v1/notifications", headers=headers)
    r.raise_for_status()
    notifications = NotificationList.model_validate_json(r.text)
    print(notifications)
    notification_id = str(notifications.items[0].id)

    put_confirm = {
        "notification_id": notification_id,
        "trigger_event": True,
    }
    r = requests.put(f"{eves_url}/v1/notifications/confirm", headers=headers, json=put_confirm)
    r.raise_for_status()

    detected_event = None

    for event in client.events():
        if event.event == "ping":
            continue

        detected_event = event
        break

    assert detected_event is not None

    assert detected_event.event == "NOTIFICATION"
    event_data = json.loads(detected_event.data)
    assert "action" in event_data
    assert event_data["action"] == "UPDATED"
