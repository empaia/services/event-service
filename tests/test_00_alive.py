import requests

from .singletons import eves_url, sos_url


def test_get_alive():
    r = requests.get(f"{eves_url}/alive")
    r.raise_for_status()

    r = requests.get(f"{sos_url}/alive")
    r.raise_for_status()
