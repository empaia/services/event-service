from uuid import uuid4

import requests

from event_service.models import NotificationList

from .singletons import eves_url, sos_url


def test_get_notifications():
    random_recipient = str(uuid4())

    message = {
        "sink": "AMQP",
        "content": "Hello Rabbit!",
    }

    post_shout_out = {
        "recipient_id": random_recipient,
        "timeout_interval": 60,
        "messages": [message],
    }

    r = requests.post(f"{sos_url}/v1/shout-outs", json=post_shout_out)
    r.raise_for_status()
    shout_out = r.json()
    assert shout_out["status"] == "UNCONFIRMED"
    assert len(shout_out["messages"]) == len(post_shout_out["messages"])

    headers = {"user-id": random_recipient}
    r = requests.get(f"{eves_url}/v1/notifications", headers=headers)
    r.raise_for_status()
    notifications = NotificationList.model_validate_json(r.text)
    assert notifications.item_count == len(shout_out["messages"])
    assert len(notifications.items) == notifications.item_count
    notification_id = str(notifications.items[0].id)
    assert notification_id == shout_out["messages"][0]["id"]
    assert notifications.items[0].content == shout_out["messages"][0]["content"]

    headers = {"user-id": str(uuid4())}
    put_confirm = {"notification_id": notification_id}
    r = requests.put(f"{eves_url}/v1/notifications/confirm", headers=headers, json=put_confirm)
    assert r.status_code == 400

    headers = {"user-id": random_recipient}
    r = requests.put(f"{eves_url}/v1/notifications/confirm", headers=headers, json=put_confirm)
    r.raise_for_status()
    notifications = NotificationList.model_validate_json(r.text)
    assert len(notifications.items) == notifications.item_count
    assert notifications.item_count == len(shout_out["messages"]) - 1
    found_id = False
    for notification in notifications.items:
        if notification.id == notification_id:
            found_id = True
            break
    assert not found_id
