import jwt
import requests
import rsa

from .singletons import eves_url


def test_email_invalid_token():
    put_data = {"confirm_token": "INVALID"}
    r = requests.put(f"{eves_url}/v1/emails/confirm", json=put_data)
    print(r.text)
    assert r.status_code == 401

    payload = {
        "sub": "sub",
        "message_id": "message_id",
        "sink": "SMTP",
    }

    _, private_key = rsa.newkeys(1024)
    print(private_key)
    private_key = private_key.save_pkcs1()

    confirm_token = jwt.encode(
        payload,
        private_key,
        algorithm="RS256",
    )

    put_data = {
        "confirm_token": confirm_token,
    }
    r = requests.put(f"{eves_url}/v1/emails/confirm", json=put_data)
    print(r.text)
    assert r.status_code == 401
