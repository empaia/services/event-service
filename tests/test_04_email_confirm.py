from time import sleep
from uuid import uuid4

import requests

from .singletons import eves_url, maildev_url, sos_url


def test_email_confirm():
    random_recipient = str(uuid4())
    headers = {"user-id": random_recipient}

    message_smtp = {
        "sink": "SMTP",
        "to": "test@test.empaia.org",
        "subject": "Subject",
        "content": "Token: {{SHOUT_OUT_TOKEN}}",
    }

    message_amqp = {
        "sink": "AMQP",
        "content": f"Hello {random_recipient}!",
    }

    post_shout_out = {
        "recipient_id": random_recipient,
        "timeout_interval": 60,
        "messages": [
            message_smtp,
            message_amqp,
        ],
    }

    r = requests.delete(f"{maildev_url}/email/all")
    r.raise_for_status()

    r = requests.post(f"{sos_url}/v1/shout-outs", json=post_shout_out)
    r.raise_for_status()

    r = requests.get(f"{eves_url}/v1/notifications", headers=headers)
    r.raise_for_status()
    notifications = r.json()

    assert len(notifications["items"]) > 0
    notification = notifications["items"][0]
    assert notification["content"] == message_amqp["content"]

    sleep(1)

    r = requests.get(f"{maildev_url}/email")
    r.raise_for_status()
    data = r.json()
    assert len(data) == 1
    email = data[0]
    text_split = email["text"].split()
    assert len(text_split) == 2
    confirm_token = text_split[1]

    put_data = {
        "confirm_token": confirm_token,
        "trigger_event": True,
    }
    r = requests.put(f"{eves_url}/v1/emails/confirm", json=put_data)
    r.raise_for_status()

    r = requests.get(f"{eves_url}/v1/notifications", headers=headers)
    r.raise_for_status()
    notifications = r.json()

    if len(notifications["items"]) > 0:
        notification = notifications["items"][0]
        assert notification["content"] != message_amqp["content"]
