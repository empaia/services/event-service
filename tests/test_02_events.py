import json
from time import sleep
from uuid import uuid4

import requests
import sseclient

from .singletons import eves_url, sos_url


def test_put_direct_amqp():
    random_recipient = str(uuid4())
    headers = {
        "user-id": random_recipient,
        "Accept": "text/event-stream",
    }

    r = requests.get(f"{eves_url}/v1/events", headers=headers, stream=True)
    r.raise_for_status()
    client = sseclient.SSEClient(r)
    sleep(1)

    put_direct_amqp_wrong = {
        "data": {"event": "WRONG"},
        "routing_key": "wrong_routing_key",
    }

    put_direct_amqp = {
        "data": {
            "event": "PORTAL_APP",
            "event_data": {"action": "CREATED"},
        },
        "routing_key": random_recipient,
    }

    r = requests.put(f"{sos_url}/v1/direct/amqp", json=put_direct_amqp_wrong)
    r.raise_for_status()

    r = requests.put(f"{sos_url}/v1/direct/amqp", json=put_direct_amqp)
    r.raise_for_status()

    r = requests.put(f"{sos_url}/v1/direct/amqp", json=put_direct_amqp_wrong)
    r.raise_for_status()

    detected_event = None

    for event in client.events():
        if event.event == "ping":
            continue

        detected_event = event
        break

    assert detected_event is not None

    assert detected_event.event != put_direct_amqp_wrong["data"]["event"]
    assert detected_event.event == put_direct_amqp["data"]["event"]
    event_data = json.loads(detected_event.data)
    assert "action" in event_data
    assert event_data["action"] == put_direct_amqp["data"]["event_data"]["action"]
