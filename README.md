# Event Service

Server Sent Events (SSE) for Portal.

## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone event-service

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd event-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Set environment variables in a `.env` file.

```bash
cp sample.env .env  # edit .env if necessary
```

### Run

Start services using `docker-compose`.

```bash
docker-compose up --build -d
```

Access the interactive OpenAPI specification in a browser:

* http://localhost:8000/docs
* http://localhost:8000/v1/docs

### Stop and Remove

```bash
docker-compose down -v
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black event_service tests tests_auth
pycodestyle event_service tests tests_auth
pylint event_service tests tests_auth
pytest tests --maxfail=1  # requires service running
```
